import * as React from 'react'
import { useState, useEffect } from 'react'
import ChildComp from './ChildComp'

const App = () => {

    const [count, setCount] = useState(0)
    const [foo, setFoo] = useState("foo")

    useEffect(() => {
        console.log("新值", count)
    }, [count])

    const changeCount = () => {
        setCount(val => val + 1)
    }

    const jsx = (<div onClick={changeCount}>
        <h1>hello react</h1>
        <p>count: { count } { foo }</p>
        <ChildComp />
    </div>)

    console.log('jsx', jsx)

    return jsx
}

export default App