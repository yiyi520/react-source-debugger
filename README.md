# react-source-debugger

#### 介绍
React 18.2.0 源码调试环境搭建


#### 1. 下载源码
首先，我们从github上把源码down下来。[react源码在github的地址](https://github.com/facebook/react)为 直接git clone down下来之后，我们可以看一下源码结构。其中src目录下的package文件中就是react源码。react是用learna多包管理工具进行管理的。其中比较核心的package有react，reactDom，react-conciler。

![react源码结构](staic/react.png)


#### 2. 搭建本地环境

使用用webpack来搭建我们调试的本地环境。npm init -y 后，直接在目录下创建webpack.config.js配置文件。

``` js
const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const webpack = require("webpack")

module.exports = {
    entry: './src/index.js',
    mode: 'development',
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /.(js)|(jsx)$/,
          exclude: /node_modules/,
          use: 'babel-loader'
        }
      ]
    },
    devServer: {
      port: '3300',
      host: 'localhost',
      client: {
        overlay: {
          errors: true,
          warnings: false,
        },
      }
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: './public/index.html'
      }),
      new webpack.DefinePlugin({
        __DEV__: true,
        __PROFILE__: true,
        __EXPERIMENTAL__: true,
        __UMD__: true,
      })
    ],
    resolve: {
      alias: {
        react: path.resolve(__dirname, './packages/react'),
        shared: path.resolve(__dirname, './packages/shared'),
        scheduler: path.resolve(__dirname, './packages/scheduler'),
        'react-dom': path.resolve(__dirname, './packages/react-dom'),
        'react-reconciler': path.resolve(__dirname, './packages/react-reconciler'),
        'react-dom-bindings': path.resolve(__dirname, './packages/react-dom-bindings'),
      }
    }
  }
```

安装相关的依赖包  
npm install webpack webpack-cli webpack-dev-server babel-loader @babel/core @babel/preset-react @babel/preset-flow html-webpack-plugin

这里因为react源码采用的是flow规范，因此我们需要下载@babel/preset-flow预设。  

项目结构图  
![项目结构](staic/structure.png)

babel配置如下
```js
module.exports = {
  presets: [
    require.resolve('@babel/preset-react'),
    require.resolve('@babel/preset-flow')
  ]
}
```

#### 3. 引入react源码
将源码中的react react-dom react-reconciler react-dom-bindings scheduler shared这五个包拷贝到项目的package目录中。

#### 4. 修改react源码
1. 修改 src\packages\react-reconciler\src\ReactFiberHostConfig.js, 导出HostConfig
    ```js
    // import invariant from 'shared/invariant';
    // invariant(false, 'This module must be shimmed by a specific renderer.');

    export * from './forks/ReactFiberHostConfig.dom'
    ```

2. 修改 src\react\packages\shared\ReactSharedInternals.js
    ```js
    // import * as React from 'react';

    // const ReactSharedInternals =
    //   React.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;
    import ReactSharedInternals from '../react/src/ReactSharedInternals'
    export default ReactSharedInternals;
    ```

#### 5. 启动项目
npm start启动项目后，就可以直接进行断点调试了。
```json
  "scripts": {
      "start": "webpack-dev-server"
  },
```
![源码debugger](staic/debugger.png)

[参考文献：原文](https://www.jianshu.com/p/0bdd1950ee5b)

